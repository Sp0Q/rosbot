# Rosbot

This repository is a collection of [scripts](./scripts) and Docker configuration. It is designed in such a way as to be usable locally, in a development mode, as well as in production via configuration to point at existing infrastructure.

To use it in development, you first need to build the rosbot container. Once the migration to public gitlab has been completed, this step will become easier, but for now you must run:

```bash
    docker-compose up
```

Once the containers are started, you will have a local rosbot, redis, and rocketchat.

Because this docker-compose setup includes a bind-mount for the scripts directory, you can immediately start working on the scripts locally, and testing them out in your local copy of rocketchat (http://localhost:3000)!

## Building

In the event that you need to build the container itself, the suggested method is:

```bash
SSH_PRIVATE_KEY=$(cat ~/your/ssh/key) ./build_container
```

which will build a container and tag it as `rosbot`. The easiest way to run the custom rosbot is to update the docker-compose file to point to rosbot rather than the upstream image.

## Usage

To run this bot without docker-compose, you should clone this repository, `cd` into it, and run:

```
docker run -it --rm -e ROCKETCHAT_URL=$ROCKETCHAT_URL \
    -e ROCKETCHAT_ROOM='' \
    -e LISTEN_ON_ALL_PUBLIC=true \
    -e ROCKETCHAT_USER=rosbot \
    -e ROCKETCHAT_PASSWORD=$ROCKETCHAT_PASSWORD \
    -e ROCKETCHAT_AUTH=password \
    -e EXTERNAL_SCRIPTS=hubot-pugme,hubot-help \
    -e HUBOT_LOG_LEVEL=debug \
    -e RESPOND_TO_DM=true \
    -e RESPOND_TO_EDITED=true \
    -e EXTERNAL_SCRIPTS=hubot-help,hubot-seen,hubot-links,hubot-diagnostics,hubot-pugme \
    -v $PWD/scripts:/home/hubot/scripts \
    --network=host \
    registry.gitlab.com/radicallyopensecurity/rosbot:latest
```

`EXTERNAL_SCRIPTS` can be modified as desired to add/remove additional hubot scripts.

Any javascript packages that are required due to imports in the scripts directory should be included via a modification of the `ADDITIONAL_PACKAGES` environment variable, which is set to `mysql,lodash` in the initial release of rosbot.

A further example can be found in [run_rosbot](./run_rosbot).

## Development

Development of rosbot scripts is done in Javascript, as Hubot v3 no longer supports coffeescript. One of the long term design goals for rosbot is to migrate the helper scripts into separate packages so that we can remove the need to clone this repository as a part of the deployment, instead leveraging local packages maintained by RadicallyOpenSecurity.

Until the above wish comes true, the development and production environments for rosbot look strikingly similar, with the scripts directory bind-mounted into the running container to consume our in-house hubot scripts.

## Testing

When a commit is made to this repository, a series of tests are run on the scripts directory, including lint and unit tests. Once those pass, a docker container is built and becomes available in the registry on gitlab.com.

## Todo

- [ ] Create pentext container, that isn't dependant on hubot
- [ ] create ros-python container, that isn't dependant on hubot

The above steps will likely start as layers that rosbot builds on, such that we end up with a build chain that looks like:

1. FROM registry.gitlab.com/radicallyopensecurity/hubot-rocketchat:latest -> Pentext
1. FROM Pentext -> ros-python
1. FROM ros-python -> rosbot

In this way, we will reduce our build time based on the fact that pentext changes infrequently, ros-python changes more frequently, and rosbot can change very often without long rebuilds being necessary.

This flow will be re-evaluated over time as wee better understand rosbot's needs.

The longer term goal for these containers could be to have pentext be available as an internal HTTP API, that rosbot can consume via a container link, such that there is an isolated pentext container that has nothing shared with the rosbot container, other than having a container link. Additionally, ros-python could, one day, become a remote (although internal, authenticated-only) API that is consumable by our other services, including rosbot.