'use strict'

/* global describe, beforeEach, afterEach, it, context */

const Helper = require('hubot-test-helper')
const chai = require('chai')

const expect = chai.expect

const helper = new Helper('../scripts/pinky.js')

describe('pinky', () => describe('Bot alertness', () => {
  beforeEach(() => {
    this.room = helper.createRoom({
      httpd: false
    })
  })

  afterEach(() => {
    this.room.destroy()
  })

  context('The bot os consulted', () => it('responds in a highly philosophical manner', (done) => {
    this.room.user.say('alice', '@hubot are you thinking what I am thinking?').then(() => {
      expect(this.room.messages[0]).to.eql(['alice', '@hubot are you thinking what I am thinking?'])
      expect(this.room.messages[1][0]).to.eql('hubot')
    }).then(done, done)
  }))
}))
