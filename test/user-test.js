'use strict'

/* global describe, it */

const assert = require('assert')
const sinon = require('sinon')
const user = require('../scripts/user.js')
const mysql = require('mysql')

describe('user', () =>
  it('queries for a user', function () {
    const mock = sinon.mock(mysql)
    mock.expects('createConnection').returns({
      connect () {},
      end () {},
      query (query, parts, cb) {
        assert.strictEqual(query, 'SELECT gitlab_id FROM user WHERE rocketchat_id = ? LIMIT 1;')
        assert.deepStrictEqual(parts, ['test'])
        cb(null, [{gitlab_id: 2}], ['gitlab_id'])
      }
    })
    user.gitlab_id_from_rocketchat_id('test', gitlabId => assert.strictEqual(gitlabId, 2))
    mock.restore()
  })
)
