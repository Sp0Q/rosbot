'use strict'

/* global describe, beforeEach, afterEach, it, context */

const Helper = require('hubot-test-helper')
const { expect } = require('chai')
const nock = require('nock')
const co = require('co')
const helper = new Helper('../scripts/project.js')

// test status
describe('status', function () {
  beforeEach(function () {
    this.room = helper.createRoom({httpd: false})
  })
  afterEach(function () {
    this.room.destroy()
  })

  context('without GITLAB_TOKEN', () =>
    // response
    it('returns the response', function () {
      this.room.user.say('alice', '@hubot project status test-project-2').then(() => {
        expect(this.room.messages).to.eql([
          ['alice', '@hubot project status test-project-2'],
          ['hubot', "@alice Cannot talk to gitlab, don't have permission (need to set $GITLAB_TOKEN)"]
        ])
      })
    })
  )

  context('without GITWEB', () =>
    // response
    it('returns the response', function () {
      process.env.GITLAB_TOKEN = 'test1234'
      this.room.user.say('alice', '@hubot project status test-project-2').then(() => {
        expect(this.room.messages).to.eql([
          ['alice', '@hubot project status test-project-2'],
          ['hubot', "@alice Cannot talk to gitlab, don't have gitserver name (need to set $GITWEB)"]
        ])
      })
    })
  )

  // Test case
  return context('user queries the status of a project', function () {
    beforeEach(function () {
      process.env.GITLAB_TOKEN = 'test1234'
      process.env.GITWEB = 'https://gitlabs.radicallyopensecurity.com'
      nock('https://gitlabs.radicallyopensecurity.com')
        .get('/api/v4/projects/8/repository/files/To_Do_List?ref=master')
        .reply(200, {
          'file_name': 'To_Do_List',
          'file_path': 'To_Do_List',
          'size': 27097,
          'encoding': 'base64',
          'content': 'dGVzdC1wcm9qZWN0LTE6IHRoZSBjdXJyZW50IHN0YXR1cyBpcyBncmVhdDsNCnRlc3QtcHJvamVjdC0yOiB0aGUgY3VycmVudCBzdGF0dXMgaXMgYW1hemluZzsNCnRlc3QtcHJvamVjdC0zOiB0aGUgY3VycmVudCBzdGF0dXMgaXMgYXdlc29tZTs=',
          'ref': 'master',
          'blob_id': '50710645a4c675b58832a50330949a220254042c',
          'commit_id': '1af2d71881079abdf2efe2cb7d9fece9dd25ce37',
          'last_commit_id': '1af2d71881079abdf2efe2cb7d9fece9dd25ce37'
        })
      return co(function * () {
        yield this.room.user.say('alice', '@hubot project status test-project-2')
        // delay one second here
        yield new Promise(function (resolve, reject) {
          setTimeout(resolve, 50)
        })
      }.bind(this))
    })

    // response
    it('returns the response', function () {
      expect(this.room.messages).to.eql([
        ['alice', '@hubot project status test-project-2'],
        ['hubot', '@alice test-project-2: the current status is amazing;\r']
      ])
    })
  })
})
