'use strict'

/* global describe, beforeEach, afterEach, it, context */

const Helper = require('hubot-test-helper')
const { expect } = require('chai')
const assert = require('assert')
const co = require('co')
const sinon = require('sinon')
const mockSpawn = require('mock-spawn')
const helper = new Helper('../scripts/bugs.js')

describe('features', () => {
  beforeEach(() => {
    this.room = helper.createRoom({httpd: false})

    const childProcess = require('child_process')
    this.mySpawn = mockSpawn()
    childProcess.spawn = this.mySpawn

    this.mock = sinon.mock(require('mysql'))
    this.mock.expects('createConnection').returns({
      connect () {},
      end () {},
      query (query, parts, cb) {
        assert.strictEqual(query, 'SELECT gitlab_id FROM user WHERE rocketchat_id = ? LIMIT 1;')
        assert.deepStrictEqual(parts, ['alice'])
        cb(null, [{gitlab_id: 2}], ['gitlab_id'])
      }
    })
  })

  afterEach(() => {
    this.room.destroy()
    this.mock.restore()
  })

  context('user requests a short feature', () => {
    beforeEach(() => {
      return co(function * () {
        yield this.room.user.say('alice', '@hubot feature this is a feature description')
        yield new Promise(function (resolve, reject) {
          setTimeout(resolve, 50)
        })
      }.bind(this))
    })

    it('creates a feature', () => {
      expect(this.room.messages).to.eql([
        ['alice', '@hubot feature this is a feature description']
      ])
      const firstCall = this.mySpawn.calls[0]
      assert.strictEqual('gitlab', firstCall.command)
      assert.deepStrictEqual(['--fancy', 'project-issue', 'create', '--project-id=617', '--title', 'this is a feature description', '--labels', 'feature', '--sudo', 2], firstCall.args)
    })
  })

  context('user requests a long feature', () => {
    beforeEach(() => {
      return co(function * () {
        yield this.room.user.say('alice', "@hubot feature this is a longer issue discussion that should wrap into title + description as it's longer than 80 characters")
        yield new Promise(function (resolve, reject) {
          setTimeout(resolve, 50)
        })
      }.bind(this))
    })

    it('creates a feature', () => {
      expect(this.room.messages).to.eql([
        ['alice', "@hubot feature this is a longer issue discussion that should wrap into title + description as it's longer than 80 characters"]
      ])
      const firstCall = this.mySpawn.calls[0]
      assert.strictEqual('gitlab', firstCall.command)
      assert.deepStrictEqual(['--fancy', 'project-issue', 'create', '--project-id=617', '--title', 'this is a longer issue discussion that should wrap into title + description as i', '--labels', 'feature', '--sudo', 2, '--description', "this is a longer issue discussion that should wrap into title + description as it's longer than 80 characters"], firstCall.args)
    })
  })
})
