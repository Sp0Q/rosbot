'use strict'

const Q = require('q')

const _ = require('lodash')

module.exports = function (robot) {
  robot.router.post('/hubot/githook/:room', function (req, res) {
    const { room } = req.params
    const data = (req.body.payload != null) ? JSON.parse(req.body.payload) : req.body
    robot.messageRoom(room, 'I just received a message from gitlabs: ')
    robot.messageRoom(room, data.secret)
    res.send('OK')
  })

  robot.router.post('/hubot/webhook/:room', function (req, res) {
    const { room } = req.params
    const data = (req.body.payload != null) ? JSON.parse(req.body.payload) : req.body
    robot.messageRoom(room, 'I just received a message from a web callback: ')
    robot.messageRoom(room, data.secret)
    res.send('OK')
  })

  const runCmd = function (cmd, args, cb) {
    console.log('spawn!')
    const { spawn } = require('child_process')
    const child = spawn(cmd, args)
    child.stdout.on('data', buffer => cb(buffer.toString()))
    child.stderr.on('data', buffer => cb(buffer.toString()))
  }

  robot.respond(/charge (.*)/i, function (msg) {
    const cmd = '/usr/bin/charge'
    // console.log(Object(msg))
    let roomName = ''
    let desc, source
    roomName = msg.message.room

    const params = _.split(_.trim(msg.match[1]), /\s+/)
    let responseMsg = params.length < 2 ? 'Enter exactly hour and description' : ''
    if ((roomName === undefined) && ((params[1].substring(0, 4) === 'pen-') || (params[1].substring(0, 4) === 'off-') || (params[1].substring(0, 4) === 'ros-'))) {
      // user is charging in a DM with rosbot and provides a channel name as a description; route channel name to proper var
      roomName = params[1]
      source = 'private'
      desc = _.join(_.slice(params, 2), ' ')
    } else if (roomName === undefined) {
      // user is charging in a DM with rosbot but is not providing a channel name as a description
      roomName = 'Rosbot DM'
      source = 'private'
      desc = _.join(_.slice(params, 1), ' ')
    } else {
      desc = _.join(_.slice(params, 1), ' ')
      source = 'public'
    }

    if (responseMsg !== '') {
      msg.reply(responseMsg)
      return
    }

    const preHours = _.toNumber(params[0])

    const hours = _.isNumber(preHours) && (preHours > 0) ? preHours : false

    responseMsg = !hours ? 'Enter valid hour' : ''

    // if response_msg != '' then msg.reply( response_msg )
    if (responseMsg !== '') {
      msg.reply(responseMsg)
      return
    }

    // args = [msg.message.room, msg.envelope.user.name, msg.match[0] , msg.match[1] ]
    const args = [ roomName, msg.envelope.user.name, hours, source, desc ]
    if (responseMsg === '') {
      runCmd(cmd, args, text => msg.send(text.replace('\n', '')))
    }
  })

  robot.router.post('/hubot/automessage/:room', function (req, res) {
    const { room } = req.params
    console.log(req.body)
    const data = (req.body.payload != null) ? JSON.parse(req.body.payload) : req.body
    console.log(data)
    robot.messageRoom(room, data.msg)

    res.send('OK')
  })

  robot.router.post('/hubot/automessagedirect/:user', function (req, res) {
    const { user } = req.params
    const data = (req.body.payload != null) ? JSON.parse(req.body.payload) : req.body
    let channel = robot.adapter.chatdriver.getDirectMessageRoomId(user)
    channel = robot.adapter.chatdriver.getDirectMessageRoomId(user)
    Q(channel)
      .then(chan => {
        robot.messageRoom(chan.rid, data.msg)
      })
      .catch(err => {
        console.log(`Unable to get DirectMessage Room ID: ${JSON.stringify(err)} Reason: ${err.reason}`)
      })
    res.send('OK')
  })
}
