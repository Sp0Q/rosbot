'use strict'

// These commands have been deprecated.

const maintenance = require('../scripts/maintenance.js')

module.exports = function (robot) {
  robot.respond(/xkcd(\s+latest)?$/i, maintenance.deprecate)

  robot.respond(/xkcd\s+(\d+)/i, maintenance.deprecate)

  robot.respond(/xkcd\s+random/i, maintenance.deprecate)

  robot.respond(/ship\s*it(.*)/i, {id: 'chatops.shipit'}, maintenance.deprecate)

  robot.respond(/sendinvoice(.*)/i, {id: 'chatops.sendinvoice'}, maintenance.deprecate)

  robot.respond(/you are a little slow/, maintenance.deprecate)

  robot.respond(/make me a channel (.*)/i, {id: 'chatops.channelcreation'}, maintenance.deprecate)

  robot.respond(/kanboard (.*)/i, maintenance.deprecate)

  robot.respond(/kanboardcomment (.*)/i, maintenance.deprecate)

  robot.respond(/kanboardinfo (.*)/i, maintenance.deprecate)

  robot.respond(/chargelist/i, maintenance.deprecate)

  robot.respond(/make an issue (.*)/i, maintenance.deprecate)

  robot.respond(/cvesearch (.*)/i, maintenance.deprecate)

  robot.respond(/cve (.*)/i, maintenance.deprecate)

  robot.respond(/kanboardstatus/i, maintenance.deprecate)

  robot.respond(/archive the chat log for (.*)/i, {id: 'chatops.chatlog'}, maintenance.deprecate)

  robot.respond(/regenerate onboarding manual/i, {id: 'chatops.shipit'}, maintenance.deprecate)

  robot.respond(/availability (.*)/i, maintenance.deprecate)

  robot.respond(/startofferte (.*)/i, {id: 'chatops.startofferte'}, res => maintenance.deprecate(res, 'This command has been deprecated, but will be reintroduced soon. Read more at https://gitlabs.radicallyopensecurity.com/ros-infra/ros-python/merge_requests/24.'))

  robot.respond(/startpentest \s*(.*)/i, {id: 'chatops.startpentest'}, res => maintenance.deprecate(res, 'This command has been deprecated, but will be reintroduced soon. Read more at https://gitlabs.radicallyopensecurity.com/ros-infra/ros-python/issues/7.'))

  robot.respond(/rainbowtables (.*)/i, maintenance.deprecate)

  robot.respond(/nmap help/i, maintenance.deprecate)

  robot.respond(/nmap (quick|full) ([a-zA-Z0-9.\-/]+)( )?([a-zA-Z0-9\-_]+)?/i, maintenance.deprecate)

  robot.respond(/sep-build (.*)/i, maintenance.deprecate)

  robot.respond(/build (.*)/i, maintenance.deprecate)

  robot.respond(/release (.*)/i, maintenance.deprecate)

  robot.respond(/convert (.*)/i, maintenance.deprecate)

  robot.respond(/invoice (.*)/i, maintenance.deprecate)

  robot.respond(/quickscope (.*)/i, maintenance.deprecate)

  robot.respond(/irquickscope (.*)/i, maintenance.deprecate)

  robot.respond(/startquote (.*)/i, maintenance.deprecate)

  robot.respond(/validate (.*)/i, maintenance.deprecate)

  robot.respond(/finalize report (["'][a-zA-z.]+ [a-zA-z.]['"])( )?([a-zA-Z0-9\-_]+)?/i, maintenance.deprecate)

  robot.respond(/crackphrase\s(.*)/i, maintenance.deprecate)
}
