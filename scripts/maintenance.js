'use strict'

function deprecate (res, message) {
  res.reply(message || 'I\'m sorry, but this command has been removed 😥 If this was by accident, and you were happily using it all this time, please file a bug report by messaging `@' + res.robot.name + ' bug ...description...`, or talk to your friendly infrastructure team in #ros-infra.')
}

function wip (res, url) {
  res.reply('I\'m sorry, but this command is under construction 👷 You can find out more at ' + url + ', or talk to your friendly infrastructure team in #ros-infra.')
}

module.exports = { deprecate, wip }
